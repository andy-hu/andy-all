##工程目录
-   common 顶级公共工程，公用的工具类和枚举等
-   platform-dao platform项目dao子项目，依赖common
-   platform-service platform项目service子项目，依赖dao
-   thread-demo  多线程编码实例
##### distributed-lock-db
基于数据库的分布式锁
-	基于数据库表
-	基于数据库排它锁
##### distributed-kit
-	基于redis实现的分布式锁
-	基于zk实现的分布式锁