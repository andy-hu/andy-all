package org.andy.pool;

import java.util.HashSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 线程池实现<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/3/9 上午10:37
 */
public class ThreadPoolExecutor implements Executor {

    /**
     * 任务队列
     */
    private final BlockingQueue<Runnable> workQueue;

    /**
     * 工作线程的容器
     */
    private final HashSet<Worker> workers ;
    private final AtomicInteger threadNumber = new AtomicInteger(1);

    public ThreadPoolExecutor(int corePoolize,BlockingQueue<Runnable> workQueue) {
        workers = new HashSet<Worker>(corePoolize);
        this.workQueue = workQueue;
    }

    @Override
    public void execute(Runnable command) {

    }

    class Worker extends Thread {
        private boolean isRunning = true;

        @Override
        public void run() {
            super.run();
        }
    }

}
