package org.andy.pool;

/**
 * 线程池接口<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/3/9 上午10:23
 */
public interface Executor {

    /**
     * 加入任务
     * @param command   需要执行的任务对象
     */
    void execute(Runnable command);

}
