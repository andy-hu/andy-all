package org.andy.platform.test;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/3/27 上午9:50
 */
public class A {

    public static void main(String[] args) {
        String original = "BBAAracarAABB";
        String reverse = "";

        int length = original.length();

        for (int i = length - 1; i >= 0; i-- ) {
            reverse = reverse + original.substring(i, i+1);
        }
        System.out.println(reverse);

        if (original.equals(reverse))
            System.out.println("Entered string is a palindrome.");
        else
            System.out.println("Entered string isn't a palindrome.");
        //
        //String original = "racar";
        //for (int i = original.length() - 1; i >= 0; i -- ) {
        //    System.out.println();
        //    String a = original.substring(i, i + 1);
        //    System.out.println(a);
        //}
    }

}
