package org.andy.kit.lock;


/**
 * 锁模板<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/8 下午12:17
 */
public interface LockTemplate {

    /**
     *  执行业务
     *
     * @param lockId    锁id(对应业务id)
     * @param timeout   超时时间
     * @param callback  回调
     * @return
     */
    Object execute(String lockId, long timeout, Callback callback);

}
