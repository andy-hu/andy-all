package org.andy.kit.lock;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/8 下午12:16
 */
public interface Callback {

    /**
     * 获取锁之后调用的业务
     * @return  Object
     */
    Object onGetLock();

    /**
     * 超时处理
     * @return  Object
     */
    Object onTimeout();

}
