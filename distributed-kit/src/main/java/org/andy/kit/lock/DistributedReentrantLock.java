package org.andy.kit.lock;

import java.util.concurrent.TimeUnit;

/**
 * 锁接口<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/8 下午12:07
 */
public interface DistributedReentrantLock {

    /**
     * 尝试获取锁
     *
     * @param timeout   超时时间
     * @param unit  时间单位
     * @return  boolean
     */
    boolean tryLock(long timeout, TimeUnit unit);

    /**
     * 释放锁
     */
    void unlock();

}
