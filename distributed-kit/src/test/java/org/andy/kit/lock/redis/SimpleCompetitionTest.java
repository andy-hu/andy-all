package org.andy.kit.lock.redis;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

import org.andy.kit.lock.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import redis.clients.jedis.JedisPool;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/9 下午1:59
 */
public class SimpleCompetitionTest {

    private static final Logger LOG = LoggerFactory.getLogger(SimpleCompetitionTest.class);

    @Test
    public void doLockSimpleCompetitionTest() {

        LOG.info("doLockSimpleCompetitionTest start" + Thread.currentThread().getId());

        JedisPool jedisPool = new JedisPool("127.0.0.1", 6379);//实际应用时可通过spring注入
        RedisReentrantLock lock = new RedisReentrantLock(jedisPool, "111");
        try {
            if (lock.tryLock(5000L, TimeUnit.MILLISECONDS)) {
                LOG.info(" doLockSimpleCompetitionTest do something.....");
                LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(40000));
            } else {
                LOG.info(" doLockSimpleCompetitionTest 超时了.....");
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        } finally {
            LOG.info("doLockSimpleCompetitionTest 释放锁 .....");
            lock.unlock();
        }
    }

    //public static void test1() {
    //    JedisPool jedisPool = new JedisPool("127.0.0.1", 6379);//实际应用时可通过spring注入
    //    final RedisLockTemplate template = new RedisLockTemplate(
    //        jedisPool);//本类多线程安全,可通过spring注入
    //    template.execute("111", 5000, new Callback() {
    //        @Override
    //        public Object onGetLock() {
    //            //TODO 获得锁后要做的事
    //            LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(30000));
    //            return null;
    //        }
    //
    //        @Override
    //        public Object onTimeout() {
    //            //TODO 获得锁超时后要做的事
    //            LOG.info("超时了.....");
    //            return null;
    //        }
    //    });
    //}

}
