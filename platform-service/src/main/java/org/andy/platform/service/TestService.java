package org.andy.platform.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/6 下午12:18
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class TestService {

    private static final Logger LOG = LoggerFactory.getLogger(TestService.class);

    public void test(){
        System.out.println("业务方法");
        try {
            Thread.sleep(10000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
