package org.andy.platform.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 〈一句话功能简述〉<br> 
 * @author andy.hu
 * @date 18/2/6 下午12:29
 * @version V1.0
 */
@Component
@Aspect
public class MethodTimeMonitor {
	
	private static Logger logger = LoggerFactory.getLogger("MethodTime");

	@Pointcut("execution(* org.andy.platform.service.TestService.test())")
    private void pointCutMethod() {    
    }

    /**
     * 申明环绕通知
     *
     * @param pjp   连接点
     * @return  Object
     * @throws Throwable
     */
    @Around("pointCutMethod()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        long begin = System.nanoTime();  
        Object o = pjp.proceed();    
        long end = System.nanoTime();  
        logger.info("{}:{}",pjp.getTarget().getClass()+"."+pjp.getSignature().getName(),"接口执行时间："+(end-begin)/1000000);  
        return o;    
    } 
}
