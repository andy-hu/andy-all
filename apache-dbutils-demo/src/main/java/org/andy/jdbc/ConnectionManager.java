package org.andy.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

import org.apache.commons.dbutils.DbUtils;

/**
 * 连接管理<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/7 上午12:27
 */
public class ConnectionManager {

    public static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(
                "jdbc:mysql://localhost/test?useUnicode=true&characterEncoding=utf-8",
                "root",
                "");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }
}
