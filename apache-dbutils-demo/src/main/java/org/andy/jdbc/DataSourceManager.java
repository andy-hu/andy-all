package org.andy.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/7 上午1:39
 */
public class DataSourceManager {


    private static DataSource dataSource = new DataSource();

    // 初始化数据库池
    static {
        PoolProperties p = new PoolProperties();
        p.setUrl("jdbc:mysql://localhost/test?useUnicode=true&characterEncoding=utf-8");
        p.setDriverClassName("com.mysql.jdbc.Driver");
        p.setUsername("root");
        p.setPassword("");
        // p.setDefaultAutoCommit(false);
        dataSource.setPoolProperties(p);
    }

    public static DataSource getDataSource() {
        return dataSource;
    }

    /**
     * 获取连接
     *
     * @return Connection
     */
    public Connection getConnection(){
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            System.out.println("获取连接时报");
            throw new RuntimeException("getConnection fail");
        }
    }
}
