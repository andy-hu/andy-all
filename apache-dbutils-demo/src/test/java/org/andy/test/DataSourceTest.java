package org.andy.test;

import java.sql.SQLException;
import java.util.List;

import org.andy.entity.MethodLock;
import org.andy.jdbc.DataSourceManager;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.junit.Test;

/**
 * 测试数据访问<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/7 上午12:56
 */
public class DataSourceTest {

    private static final QueryRunner QUERY_RUNNER = new QueryRunner(DataSourceManager.getDataSource());

    @Test
    public void testInsert(){
        final String SQL = "insert method_lock(method_name, description) values(?,?)";
        try {
            QUERY_RUNNER.update(SQL, new Object[]{"method111","asc"});
        } catch (SQLException e) {
            System.out.println("sql执行错...");
            e.printStackTrace();
        }
    }

    @Test
    public void testQuery(){
        final String SQL = "select id,method_name as methodName, description, update_time as updateTime  from method_lock where id = 1";
        try {
            MethodLock methodLock = QUERY_RUNNER.query(SQL, new BeanHandler<>(MethodLock.class));
            if (null != methodLock){
                System.out.println(methodLock.getId());
                System.out.println(methodLock.getMethodName());
            }
        } catch (SQLException e) {
            System.out.println("sql执行错...");
            e.printStackTrace();
        }
    }

    @Test
    public void testQueryList(){
        final String SQL = "select id,method_name as methodName, description, update_time as updateTime from method_lock";
        try {
            List<MethodLock> list = QUERY_RUNNER.query(SQL, new BeanListHandler<>(MethodLock.class));
            if (null != list) {
                System.out.println(list.size());
                for (MethodLock methodLock : list) {
                    System.out.println(methodLock.toString());
                }
            }

        } catch (SQLException e) {
            System.out.println("sql执行错...");
            e.printStackTrace();
        }
    }

    @Test
    public void testUpdate(){
        final String SQL = "update method_lock set description = '22' where id = ?";
        try {
            int result = QUERY_RUNNER.update(SQL, new Object[]{1});
            System.out.println(result);
        } catch (SQLException e) {
            System.out.println("sql执行错...");
            e.printStackTrace();
        }
    }

    @Test
    public void testDelete(){
        final String SQL = "delete from method_lock where id = ?";
        try {
            int result = QUERY_RUNNER.update(SQL, new Object[]{2});
            System.out.println(result);
        } catch (SQLException e) {
            System.out.println("sql执行错...");
            e.printStackTrace();
        }
    }

}
