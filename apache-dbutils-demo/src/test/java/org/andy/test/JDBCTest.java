package org.andy.test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.andy.entity.MethodLock;
import org.andy.jdbc.ConnectionManager;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.junit.Test;

/**
 * 测试数据访问<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/7 上午12:56
 */
public class JDBCTest {

    private static final QueryRunner QUERY_RUNNER = new QueryRunner();

    @Test
    public void testInsert(){
        final String SQL = "insert method_lock(method_name, description) values(?,?)";
        Connection conn = ConnectionManager.getConnection();
        try {
            QUERY_RUNNER.update(conn, SQL, new Object[]{"method1","asc"});
        } catch (SQLException e) {
            System.out.println("sql执行错...");
            e.printStackTrace();
        }finally {
            close(conn);
        }
    }

    @Test
    public void testQuery(){
        final String SQL = "select * from method_lock where id = 1";
        Connection conn = ConnectionManager.getConnection();
        try {
            MethodLock methodLock = QUERY_RUNNER.query(conn, SQL, new BeanHandler<>(MethodLock.class));
            if (null != methodLock){
                System.out.println(methodLock.getId());
                System.out.println(methodLock.getMethodName());
            }
        } catch (SQLException e) {
            System.out.println("sql执行错...");
            e.printStackTrace();
        }finally {
            close(conn);
        }
    }

    @Test
    public void testQueryList(){
        final String SQL = "select * from method_lock";
        Connection conn = ConnectionManager.getConnection();
        try {
            List<MethodLock> list = QUERY_RUNNER.query(conn, SQL, new BeanListHandler<>(MethodLock.class));
            if (null != list) {
                System.out.println(list.size());
                for (MethodLock methodLock : list) {
                    System.out.println(methodLock.toString());
                }
            }

        } catch (SQLException e) {
            System.out.println("sql执行错...");
            e.printStackTrace();
        }finally {
            close(conn);
        }
    }

    @Test
    public void testUpdate(){
        final String SQL = "update method_lock set description = '22' where id = ?";
        Connection conn = ConnectionManager.getConnection();
        try {
            int result = QUERY_RUNNER.update(conn, SQL, new Object[]{1});
            System.out.println(result);
        } catch (SQLException e) {
            System.out.println("sql执行错...");
            e.printStackTrace();
        }finally {
            close(conn);
        }
    }

    @Test
    public void testDelete(){
        final String SQL = "delete from method_lock where id = ?";
        Connection conn = ConnectionManager.getConnection();
        try {
            int result = QUERY_RUNNER.update(conn, SQL, new Object[]{2});
            System.out.println(result);
        } catch (SQLException e) {
            System.out.println("sql执行错...");
            e.printStackTrace();
        }finally {
            close(conn);
        }
    }



    public void close(Connection conn){
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
