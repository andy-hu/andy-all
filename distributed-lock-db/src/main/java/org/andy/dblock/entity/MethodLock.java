package org.andy.dblock.entity;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/7 上午1:09
 */
public class MethodLock {

    private Long id;
    private String methodName;
    private String description;
    private String updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "MethodLock{" +
            "id=" + id +
            ", methodName='" + methodName + '\'' +
            ", description='" + description + '\'' +
            ", updateTime='" + updateTime + '\'' +
            '}';
    }
}
