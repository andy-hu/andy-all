package org.andy.dblock.manager;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/7 上午1:39
 */
public class DataSourceManager {

    private static final Logger LOG = LoggerFactory.getLogger(DataSourceManager.class);

    /**
     * 线程本地变量
     */
    private static final ThreadLocal<Connection> CONNECTION = new ThreadLocal<>();

    private static DataSource dataSource = new DataSource();

    // 初始化数据库池
    static {
        PoolProperties p = new PoolProperties();
        p.setUrl("jdbc:mysql://localhost/test?useUnicode=true&characterEncoding=utf-8");
        p.setDriverClassName("com.mysql.jdbc.Driver");
        p.setUsername("root");
        p.setPassword("");
        // p.setDefaultAutoCommit(false);
        dataSource.setPoolProperties(p);
    }

    public static DataSource getDataSource() {
        return dataSource;
    }

    /**
     * 获取连接
     *
     * @return Connection
     */
    public static Connection getConnection() {
        Connection conn;
        try {
            conn = CONNECTION.get();
            if (conn == null) {
                conn = getDataSource().getConnection();
                if (conn != null) {
                    CONNECTION.set(conn);
                }
            }
        } catch (SQLException e) {
            LOG.error("get connection exception [{}]", e.getMessage());
            throw new RuntimeException("getConnection failure");
        }
        return  conn;
    }

    /**
     * 开始事务
     */
    public static void beginTransaction(){
        Connection conn = getConnection();
        if (conn != null){
            try {
                conn.setAutoCommit(false);
            } catch (SQLException e) {
                LOG.error("begin transaction failure [{}]", e.getMessage());
                throw new RuntimeException("begin transaction failure");
            }finally {
                CONNECTION.set(conn);
            }
        }
    }

    /**
     * 提交事务
     */
    public static void commitTeansaction(){
        Connection conn = getConnection();
        if (conn != null){
            try {
                conn.commit();
                conn.close();
            } catch (SQLException e) {
                LOG.error("commit transaction failure [{}]", e.getMessage());
                throw new RuntimeException("commit transaction failure");
            }finally {
                CONNECTION.remove();
            }
        }
    }

    public static void rollbackTransaction(){
        Connection conn = getConnection();
        if (conn != null){
            try {
                conn.rollback();
                conn.close();
            } catch (SQLException e) {
                LOG.error("rollback transaction failure [{}]", e.getMessage());
                throw new RuntimeException("rollback transaction failure");
            }finally {
                CONNECTION.remove();
            }
        }
    }
}
