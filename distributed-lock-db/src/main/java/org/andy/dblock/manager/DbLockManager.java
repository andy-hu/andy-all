package org.andy.dblock.manager;

/**
 * 数据库锁接口<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/7 上午12:25
 */
public interface DbLockManager {

    /**
     * 获取锁/加锁
     * @return  boolean
     */
    boolean lock(String methodName);

    /**
     * 释放锁
     */
    void unLock(String methodName);

}
