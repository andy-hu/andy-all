package org.andy.test.transaction;

import java.sql.SQLException;

import org.andy.dblock.manager.DataSourceManager;
import org.andy.util.UUIDGenerator;
import org.apache.commons.dbutils.QueryRunner;
import org.junit.Test;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/8 上午12:56
 */
public class TransactionTest {

    private static final QueryRunner QUERY_RUNNER = new QueryRunner(DataSourceManager.getDataSource());

    public static String insertSql = "insert method_lock(method_name, description) values(?,?)";


    @Test
    public void test(){
        String methodName = UUIDGenerator.getUUID();
        try {
            DataSourceManager.beginTransaction();
            int result = QUERY_RUNNER.update(DataSourceManager.getConnection(),insertSql, new Object[] {methodName, "desc"});

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DataSourceManager.commitTeansaction();
        }
    }

}
