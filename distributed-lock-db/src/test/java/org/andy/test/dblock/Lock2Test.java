package org.andy.test.dblock;

import org.andy.dblock.manager.DbLockManager;
import org.andy.dblock.manager.impl.DbLockManagerImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/7 下午4:20
 */
public class Lock2Test {

    private static final Logger LOG = LoggerFactory.getLogger(Lock2Test.class);


    @org.testng.annotations.Test
    public void getLock2(){
        DbLockManager dbLockManager = new DbLockManagerImpl();
        String methodName = "method_name_test";
        boolean isLock = false;


        try {
            while (!isLock){
                isLock = dbLockManager.lock(methodName);
                Thread.sleep(1000L);
                System.out.println("getLock2尝试获取锁....");
            }

            System.out.println("getLock2获取锁成功....");

            // do业务 60秒
            Thread.sleep(6000L);

        }catch (Exception e){
            LOG.error("getLock2 methodName = {},{}", methodName, e.getMessage());
        }finally {
            System.out.println("getLock2释放锁....");
            dbLockManager.unLock(methodName);
        }

    }

}
