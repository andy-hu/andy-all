package org.andy.test.dbexclusivelock;

import org.andy.dblock.manager.DbLockManager;
import org.andy.dblock.manager.impl.DbLockManagerImpl;
import org.andy.test.dblock.LockTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/8 上午1:18
 */
public class Lock4Test {

    private static final Logger LOG = LoggerFactory.getLogger(LockTest.class);

    @org.testng.annotations.Test
    public void getLock() {
        DbLockManager dbLockManager = new DbLockManagerImpl();
        String methodName = "method_name_test";
        boolean isLock = false;

        try {
            System.out.println("getLock 等待获取锁....");
            isLock = dbLockManager.lock(methodName);

            System.out.println("getLock 获取锁成功....");

            // do业务 60秒
            Thread.sleep(6000L);

        } catch (Exception e) {
            LOG.error("getLock methodName = {},{}", methodName, e.getMessage());
        } finally {
            dbLockManager.unLock(methodName);
            System.out.println("getLock 释放锁....");
        }

    }
}
