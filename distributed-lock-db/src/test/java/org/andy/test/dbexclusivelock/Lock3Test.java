package org.andy.test.dbexclusivelock;

import org.andy.dblock.manager.DbLockManager;
import org.andy.dblock.manager.impl.DbExclusiveLockManagerImpl;
import org.andy.test.dblock.Lock2Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/8 上午1:15
 */
public class Lock3Test {

    private static final Logger LOG = LoggerFactory.getLogger(Lock2Test.class);

    private static final String methodName = "method_name_test2" ;


    @org.testng.annotations.Test
    public void getLock2(){
        DbLockManager dbLockManager = new DbExclusiveLockManagerImpl();

        try {
            dbLockManager.lock(methodName);

            System.out.println("getLock2获取锁成功....");

            // do业务 60秒
            Thread.sleep(6000L);

        }catch (Exception e){
            LOG.error("getLock2 methodName = {},{}", methodName, e.getMessage());
        }finally {
            System.out.println("getLock2释放锁....");
            dbLockManager.unLock(methodName);
        }

    }
}
