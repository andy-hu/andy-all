package org.andy.test;

import org.andy.test.dblock.Lock2Test;
import org.andy.test.dblock.LockTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/7 下午7:41
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({LockTest.class,Lock2Test.class})
public class SuiteTest {
}
