package org.andy.platform.Controller;

import org.andy.platform.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 测试类<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/5 下午1:16
 */
@Controller
@RequestMapping(value = "/test")
public class TestController {

    @Autowired
    private TestService testService;

    @RequestMapping(value = "/example", method = {RequestMethod.POST, RequestMethod.GET},
        produces = {"application/json;charset=UTF-8"})
    @ResponseBody
    public String example() {
        testService.test();
        return "111";
    }

}
