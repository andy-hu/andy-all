package org.andy.exception;

/**
 * 没有登录异常 <br>
 *
 * @author andy.hu
 * @date 18/2/5 上午10:44
 * @version V1.0
 */
public class NotLoginException extends RuntimeException {
	
	private static final long serialVersionUID = 946812580401243454L;

	public NotLoginException(String string) {
		super(string);
	}

	public NotLoginException(String string, Throwable e) {
		super(string, e);
	}
}
