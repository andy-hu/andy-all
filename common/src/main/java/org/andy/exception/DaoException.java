package org.andy.exception;

/**
 * Dao层公用异常.
 * 继承自RuntimeException，Dao层不处理异常，全部抛出DaoException.
 *      
 * @author andy.hu
 * @date 2017年6月19日 下午6:09:59
 */
public class DaoException extends RuntimeException{

    /**TODO*/
    private static final long serialVersionUID = 2053993465233686873L;
    
    public DaoException() {
        super();
    }

    public DaoException(String message) {
        super(message);
    }

    public DaoException(Throwable cause) {
        super(cause);
    }

    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }

}
