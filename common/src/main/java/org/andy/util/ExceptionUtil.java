package org.andy.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * 异常处理工具类<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/7 下午12:09
 */
public class ExceptionUtil {

    /**
     * 输出异常的完整信息
     *
     * @param e 异常
     * @return 异常堆栈信息
     */
    public static final String getTrace(Throwable e) {
        StringWriter stringWriter = null;
        try {
            stringWriter = new StringWriter();
            PrintWriter writer = new PrintWriter(stringWriter);
            e.printStackTrace(writer);
            StringBuffer buffer = stringWriter.getBuffer();
            return buffer.toString();
        } finally {
            if (stringWriter != null) {
                try {
                    stringWriter.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

}
