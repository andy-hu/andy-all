package org.andy.util;

/**
 * 类操作相关工具类<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/7 下午12:10
 */
public class ClassUtil {

    /**
     * 获取类加载器
     *
     * @return  ClassLoader
     */
    public static ClassLoader getClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }
}
