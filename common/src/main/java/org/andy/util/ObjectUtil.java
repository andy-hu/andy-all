package org.andy.util;

/**
 * 对象操作工具类<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/2/14 下午9:32
 */
public class ObjectUtil {

    public static boolean isNull(Object obj) {
        return obj == null;
    }

    public static boolean notNull(Object obj) {
        return obj != null;
    }

}
