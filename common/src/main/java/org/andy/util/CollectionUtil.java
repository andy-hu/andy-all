package org.andy.util;

import java.util.Collection;

/**
 * 集合操作工具类<br>
 *
 * @author andy.hu
 * @date 18/2/7 下午12:19
 * @version V1.0
 */
public class CollectionUtil {

    /**
     * 判断集合是否非空
     */
    public static boolean isNotEmpty(Collection<?> collection) {
        return !isEmpty(collection);
    }

    /**
     * 判断集合是否为空
     */
    public static boolean isEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }
}
