package org.andy.queue;

/**
 * 单链表的基本数据节点<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/3/8 下午8:30
 */
public class Node<T> {

    Node<T> next;
    T data;

    public Node(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Node{" +
            "data=" + data +
            '}';
    }
}
