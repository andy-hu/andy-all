package org.andy.queue;

/**
 * 单链表实现的简单队列<br>
 * 含头结点(头结点不存储值,添加操作O(1))，尾指针(删除操作O(1))
 * @author andy
 * @version V1.0
 * @date 18/3/8 下午8:32
 */
public class LinkedBlockQueue<E> implements Queue<E>{

    // 头节点
    private Node<E> head;
    // 尾节点
    private Node<E> last;
    // 队列数量
    private int size;

    public LinkedBlockQueue() {
        this.head = last =  new Node<E>(null);
    }

    /**
     * 添加元素到队列尾
     *
     * @param e 元素
     */
    @Override
    public synchronized void put(E e) {
        Node<E> node = new Node<E>(e);
        last.next = node;
        last = node;
        size++;
    }

    /**
     * 删除队头并返回队头元素的值
     * @return e 元素
     */
    @Override
    public synchronized E pop() {
        if (size != 0){
            Node<E> temp = head.next;
            // 删除一个元素时只需要改变指针
            head.next = temp.next;
            E e = temp.data;

            temp.data = null;
            temp.next = null;
            size--;
            return e;
        }
        return null;
    }

    public synchronized int size(){
        return size;
    }
}
