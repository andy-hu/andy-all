package org.andy.queue;

/**
 * 队列接口 <br>
 *
 * @author andy
 * @version V1.0
 * @date 18/3/8 下午5:07
 */
public interface Queue<E> {

    /**
     * 添加元素到队列尾部
     *
     */
    public void put(E e);

    /**
     * 删除队头并返回队头元素E
     * @return e 元素
     */
    public E pop();


}
