package org.andy.queue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/3/19 下午10:51
 */
public class ArrayBlockQueueTest {

    ArrayBlockQueue<String> quque;

    @Before
    public void init(){
        quque = new ArrayBlockQueue<>(100);
        System.out.println("before");
        quque.put("11");
        quque.put("12");
        quque.put("13");
        quque.put("14");
        quque.put("15");
        quque.put("16");
    }

    @org.junit.Test
    public void put() {
        quque.put("17");
        assertEquals(quque.size(), 7);
    }

    @org.junit.Test
    public void pop() {
        String s = quque.pop();
        assertEquals(s, "11");
        assertEquals(quque.size(), 5);
    }

    @org.junit.Test
    public void size() {
        System.out.println(quque.size());
    }

    @org.junit.Test
    public void isFull() {
        System.out.println(quque.size());
    }

    @Test
    public void putAndPop(){
        System.out.println(quque.pop());
        System.out.println(quque.pop());
        System.out.println(quque.pop());
        System.out.println(quque.pop());
        System.out.println(quque.pop());
        System.out.println(quque.pop());
        assertEquals(quque.size(), 0);
    }
}