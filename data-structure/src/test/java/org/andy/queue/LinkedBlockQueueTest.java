package org.andy.queue;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author andy
 * @version V1.0
 * @date 18/3/19 下午11:49
 */
public class LinkedBlockQueueTest {

    LinkedBlockQueue<String> blockQueue;

    @Before
    public void init(){
        blockQueue = new LinkedBlockQueue<>();
        blockQueue.put("11");
        blockQueue.put("12");
        blockQueue.put("13");
        blockQueue.put("14");
    }

    @Test
    public void put() {
        blockQueue.put("15");
        assertEquals(blockQueue.size(), 5);
    }

    @Test
    public void pop() {
        assertEquals(blockQueue.pop(), "11");
    }

    @Test
    public void size() {
        assertEquals(blockQueue.size(), 4);
    }

    @Test
    public void putAndPop(){
        assertEquals(blockQueue.pop(), "11");
        assertEquals(blockQueue.pop(), "12");
        assertEquals(blockQueue.pop(), "13");
        assertEquals(blockQueue.pop(), "14");
    }
}